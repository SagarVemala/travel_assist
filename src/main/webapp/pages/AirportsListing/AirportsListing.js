Application.$controller("AirportsListingPageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";
    $scope.selectedAirport = 'none';
    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

    };


    $scope.mobile_navbar1Backbtnclick = function($event, $isolateScope) {
        $scope.$emit('mobile_navbar1Backbtnclick');
    };


    $scope.livelist3Tap = function($event, $isolateScope) {
        //alert($rootScope.airport)
        if ($rootScope.airport == "from") {
            $scope.Variables.selectedFromAirport.dataValue = $isolateScope.item;
            $scope.selectedAirport = $isolateScope.item.airportCode;
            //$scope.Variables.getDomesticAirports.setInput('selectedAirport', $isolateScope.item.airportCode);
            $scope.Variables.getDomesticAirports.update();
            $scope.Variables.selectedToAirport.dataValue = '';
        }
        if ($rootScope.airport == "to") {
            $scope.Variables.selectedToAirport.dataValue = $isolateScope.item;
        }
        $scope.$emit('mobile_navbar1Backbtnclick');
    };

}]);