Application.$controller("tripDetailsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */


    };



    function calcTime(createdOn) {
        var diffObj = {
            diffValue: "",
            diffUnit: ""
        };
        if (moment(createdOn).diff(moment(), 'days') < 7) {

            diffObj.diffValue = moment(createdOn).diff(moment(), 'days');
            diffObj.diffUnit = 'Days';
        } else if (moment(createdOn).diff(moment(), 'weeks') > 0) {
            diffObj.diffValue = moment(createdOn).diff(moment(), 'weeks');
            diffObj.diffUnit = 'Weeks';
        } else {
            diffObj.diffValue = ' - ';
            diffObj.diffUnit = 'Weeks';
        }
        // return createdOn;
        return diffObj;
    }


    $scope.flightDetailsonSuccess = function(variable, data) {
        $scope.Variables.SelectedFlight.dataSet.dataValue = data;
        $scope.Variables.approvedTrip.dataSet.timeDiff = calcTime(data.content[0].DEPT_DATE);

    };


    $scope.GetNearByPlacesInvokeonSuccess = function(variable, data) {
        if (data.results.length != 0) {

            $scope.Widgets.carouselPlaces.show = true;
            $scope.Widgets.NearBy.show = true;
            $scope.Widgets.containerNoPlaces.show = false;

        } else {
            $scope.Widgets.carouselPlaces.show = false;
            $scope.Widgets.NearBy.show = false;
            $scope.Widgets.containerNoPlaces.show = true;

        }
    };

}]);