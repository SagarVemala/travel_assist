/*Copyright (c) 2015-2016 wavemaker-com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker-com*/
package com.travel_advisor.travel_advisor_db;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Employee generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`EMPLOYEE`")
public class Employee implements Serializable {

    private Integer empId;
    private String password;
    private String fullName;
    private Integer managerId;
    private String picUrl;
    private String username;
    private Employee employeeByManagerId;
    private List<Employee> employeesForManagerId = new ArrayList<>();
    private List<Requests> requestses = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`EMP_ID`", nullable = false, scale = 0, precision = 10)
    public Integer getEmpId() {
        return this.empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    @Column(name = "`PASSWORD`", nullable = true, length = 255)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "`FULL_NAME`", nullable = true, length = 255)
    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Column(name = "`MANAGER_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getManagerId() {
        return this.managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    @Column(name = "`PIC_URL`", nullable = true, length = 255)
    public String getPicUrl() {
        return this.picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @Column(name = "`USERNAME`", nullable = true, length = 255)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    // ignoring self relation properties to avoid circular loops.
    @JsonIgnoreProperties({"employeeByManagerId", "employeesForManagerId"})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`MANAGER_ID`", referencedColumnName = "`EMP_ID`", insertable = false, updatable = false)
    public Employee getEmployeeByManagerId() {
        return this.employeeByManagerId;
    }

    public void setEmployeeByManagerId(Employee employeeByManagerId) {
        if(employeeByManagerId != null) {
            this.managerId = employeeByManagerId.getEmpId();
        }

        this.employeeByManagerId = employeeByManagerId;
    }

    // ignoring self relation properties to avoid circular loops.
    @JsonIgnoreProperties({"employeeByManagerId", "employeesForManagerId"})
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "employeeByManagerId")
    public List<Employee> getEmployeesForManagerId() {
        return this.employeesForManagerId;
    }

    public void setEmployeesForManagerId(List<Employee> employeesForManagerId) {
        this.employeesForManagerId = employeesForManagerId;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "employee")
    public List<Requests> getRequestses() {
        return this.requestses;
    }

    public void setRequestses(List<Requests> requestses) {
        this.requestses = requestses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        final Employee employee = (Employee) o;
        return Objects.equals(getEmpId(), employee.getEmpId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmpId());
    }
}

